package com.ToDoApp.controller.rest;

import com.ToDoApp.model.ToDo;
import com.ToDoApp.service.ToDoService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/rest/todos")
@Slf4j
@Tag(name = "Задачи (to do)", description = "Контроллер для работы с задачами") //swagger openapi настройка
public class RestToDoController {
    private final ToDoService toDoService;

    @Operation(description = "Получить список всех задач") //swagger openapi настройка
    @RequestMapping(value = "/getListOfToDos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    //тип запроса - get
    public ResponseEntity<List<ToDo>> getAllToDos() {
        return ResponseEntity.status(HttpStatus.OK)
                .body(toDoService.listAll());
    }

    @Operation(description = "Получить задачу по id")
    @RequestMapping(value = "/getToDo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    //тип запроса - GET
    public ResponseEntity<ToDo> getOneToDo(@RequestParam(value = "id") Integer id) throws NotFoundException {
        return ResponseEntity.status(HttpStatus.OK)
                .body(toDoService.getOne(id));
    }

    @Operation(description = "Добавить новую задачу")
    @RequestMapping(value = "/addToDo", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ToDo> createToDo(@RequestBody ToDo newToDo) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(toDoService.create(newToDo));
    }

    @Operation(description = "Обновить задачу по id")
    @RequestMapping(value = "/updateToDo", method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ToDo> updateToDo(@RequestBody ToDo updatedToDo, @RequestParam(value = "id") Integer id) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(toDoService.update(updatedToDo, id));
    }

    @Operation(description = "Удалить задачу")
    @RequestMapping(value = "/deleteToDo/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable(value = "id") Integer id) {
        toDoService.delete(id);
    }
}

