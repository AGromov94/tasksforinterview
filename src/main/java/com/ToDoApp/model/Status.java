package com.ToDoApp.model;

public enum Status {
    STATUS_1("Даже не начинал"),
    STATUS_2("Только начал"),
    STATUS_3("В процессе выполнения"),
    STATUS_4("Почти готово"),
    STATUS_5("Готово");

    private final String statusTextDisplay;

    Status(String text) {
        this.statusTextDisplay = text;
    }
}
