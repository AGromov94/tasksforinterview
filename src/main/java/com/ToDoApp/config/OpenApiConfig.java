package com.ToDoApp.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenApiConfig {
    @Bean
    public OpenAPI toDoProject() {
        return new OpenAPI().info(new Info()
                .title("API приложения ToDo List")
                .description("Сервич для ведения списка задач")
                .version("1.0")
                .license(new License().name("Apache 2.0").url("http://kakoitourl.org"))
                .contact(new Contact().name("Andrei Gromov")
                        .email("andrey.gromov1994@mail.ru"))
        );
    }
}
