package com.ToDoApp.service;

import com.ToDoApp.model.ToDo;
import com.ToDoApp.repository.ToDoRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.time.LocalDateTime;
import java.util.List;

@Service
@Slf4j
public class ToDoService {
    protected final ToDoRepository toDoRepository;
    protected ToDoService(ToDoRepository toDoRepository) {
        this.toDoRepository = toDoRepository;
    }

    public List<ToDo> listAll() {
        return (List<ToDo>) toDoRepository.findAll();
    }

    public ToDo getOne(Integer id) {
        return toDoRepository.findById(id).orElseThrow(() -> new NotFoundException("Нет данных по заданному id"));
    }

    public ToDo create(ToDo newToDo) {
        newToDo.setCreatedWhen(LocalDateTime.now());
        newToDo.setCreatedBy("ADMIN");
        return toDoRepository.save(newToDo);
    }

    public ToDo update(ToDo updatedToDo, Integer id) {
        updatedToDo.setUpdatedWhen(LocalDateTime.now());
        updatedToDo.setUpdatedBy("ADMIN");
        updatedToDo.setId(id);
        return toDoRepository.save(updatedToDo);
    }

    public void delete(Integer id) {
        toDoRepository.deleteById(id);
    }
}
